/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef COMMON_HH_00843BA3923777A098B9BCDF2FD9E7BA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_HH_00843BA3923777A098B9BCDF2FD9E7BA

#include "src/utf/utf8.hh"

#include <utility>

template <char ...sequence>
using Characters = std::integer_sequence<char, sequence...>;

template <template <char...> class, typename...> struct BoolAnd;

template <char ...utf8_encoded_content>
using IsCorrectUtf8ByteSequence = std::integral_constant<bool, Utf::IsCorrect<Utf::Utf8EncodedSequence<utf8_encoded_content...>>::value>;

template <char ...utf8_encoded_content>
using IsIncorrectUtf8ByteSequence = std::integral_constant<bool, !Utf::IsCorrect<Utf::Utf8EncodedSequence<utf8_encoded_content...>>::value>;

using ValidOneByteCharacters = Characters<'\0', '\n', ' ', '-', '0', '9', 'A', 'Z', 'a', 'z', '\x7f'>;
using ValidTwoByteHeads = Characters<'\xc2', '\xdf'>;
using ValidThreeByteHeads = Characters<'\xe0', '\xef'>;
using ValidFourByteHeads = Characters<'\xf0', '\xf4'>;
using ContinuationBytes = Characters<'\x80', '\xbf'>;

#endif//COMMON_HH_00843BA3923777A098B9BCDF2FD9E7BA
