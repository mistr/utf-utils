/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "src/utf/convert.hh"
#include "src/utf/utf8_cstring.hh"
#include "src/utf/utf8_length.hh"
#include "src/utf/utf32_cstring.hh"
#include "src/utf/utf32_length.hh"

#include <cstdlib>
#include <iostream>

using HelloWorld = Utf::Utf8EncodedSequence<'n', 'a', 'z', 'd', 'a', 'r', ' ', 'p', 'r', 'd', 'e'>;
using HelloWorldString = Utf::CString<HelloWorld>;

static_assert(HelloWorldString()[0] == 'n', "must be 'n'");
static_assert(HelloWorldString::at<0> == 'n', "must be 'n'");
static_assert(HelloWorldString()[11] == '\0', "must be '\\0'");
static_assert(HelloWorldString::at<11> == '\0', "must be '\\0'");
static_assert(HelloWorldString() == HelloWorldString(), "must be equal");
static_assert(HelloWorldString() == "nazdar prde", "must be equal");
static_assert("nazdar prde" == HelloWorldString(), "must be equal");

using Foo = Utf::Utf8EncodedSequence<'f', 'o', 'o'>;
using Bar = Utf::Utf8EncodedSequence<'b', 'a', 'r'>;
using FooString = Utf::CString<Foo>;
using BarString = Utf::CString<Bar>;

static_assert(FooString() != BarString(), "must be different");
static_assert(FooString().size() == BarString().length(), "must be equal");
static_assert(BarString() < FooString(), "must be smaller");
static_assert(BarString() < HelloWorldString(), "must be smaller");
static_assert(FooString() < HelloWorldString(), "must be smaller");
static_assert(!(FooString() < BarString()), "must be greater");
static_assert(!(HelloWorldString() < BarString()), "must be greater");
static_assert(!(HelloWorldString() < FooString()), "must be greater");

namespace Utf {

}//namespace Utf

using HelloWorldWString = Utf::CString<Utf::Convert<HelloWorld>::ToUtf32>;

template <typename>
void show()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template <typename T>
void show(T&&)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

auto get_test_data()
{
    struct
    {
        static constexpr const char* get()noexcept
        {
            return "Příliš žluťoučký kůň úpěl ďábelské ódy.";
        }
    } x;
    return x;
}

template <typename, typename = void> struct FromStringLiteral;

template <typename T>
struct FromStringLiteral<T, std::enable_if_t<*T::get() == '\0'>>
{
    using ToIntegerSequence = std::integer_sequence<char>;
};

template <typename T>
struct FromStringLiteral<T, std::enable_if_t<*T::get() != '\0'>>
{
private:
    struct Tail
    {
        static constexpr const char* get()noexcept
        {
            return T::get() + 1;
        }
    };
    using Prefix = std::integer_sequence<char, *T::get()>;
    using Suffix = typename FromStringLiteral<Tail>::ToIntegerSequence;
public:
    using ToIntegerSequence = typename Utf::detail::Join<Prefix, Suffix>::Result;
};

template <typename T>
constexpr auto get_string()noexcept
{
    using Content = typename FromStringLiteral<T>::ToIntegerSequence;
    using Utf8 = typename Utf::Convert<Content>::ToUtf8;
    using String = Utf::CString<Utf8>;
    return String();
}

void utf_test()
{
    using Utf8 = decltype(get_string<decltype(get_test_data())>())::Type;
    using String = Utf::CString<Utf8>;
    using WString = Utf::Convert<String>::ToWString;
    static_assert(std::is_same<Utf8, typename Utf::Convert<typename WString::Type>::ToUtf8>::value, "types must be equal");
    std::cout << "\"" << String() << "\" is " << Utf::Length<Utf8>::value << " characters length" << std::endl;
    std::cout << "\"" << WString() << "\" is " << Utf::Length<WString::Type>::value << " characters length" << std::endl;
    show<WString::Type>();
    show<decltype(get_test_data())>();
    show(decltype(get_test_data())::get());
    show(get_string<decltype(get_test_data())>());
    show<Utf::detail::Join<Utf::Utf8EncodedSequence<>, Utf::Utf8EncodedSequence<>>::Result>();
}

int main()
{
    std::cout << "\"" << HelloWorldString() << "\" is " << Utf::Length<HelloWorld>::value << " characters length" << std::endl;
    std::cout << "\"" << HelloWorldWString() << "\" is " << Utf::Length<Utf::Convert<HelloWorld>::ToUtf32>::value << " characters length" << std::endl;
    show<Utf::Convert<Utf::Utf8EncodedSequence<'a', 'b', "č"[0], "č"[1]>>::ToUtf32>();
    utf_test();
    return EXIT_SUCCESS;
}
