/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "test/common.hh"

using InvalidOneByteCharacters = Characters<'\x80', '\x90', '\xa0', '\xb0', '\xc0', '\xc1', '\xc2', '\xd0', '\xe0', '\xf0', '\xf1', '\xf3', '\xf4', '\xf5', '\xff'>;

template <template <char...> class IsTrue, char head>
struct BoolAnd<IsTrue, Characters<head>>
    : std::integral_constant<bool, IsTrue<head>::value>
{
    static_assert(IsTrue<head>::value, "must be true");
};

template <template <char...> class IsTrue, char head, char ...tail>
struct BoolAnd<IsTrue, Characters<head, tail...>>
    : std::integral_constant<bool, BoolAnd<IsTrue, Characters<head>>::value &&
                                   BoolAnd<IsTrue, Characters<tail...>>::value>
{ };

static_assert(IsCorrectUtf8ByteSequence<>::value, "must be correct");
static_assert(BoolAnd<IsCorrectUtf8ByteSequence, ValidOneByteCharacters>::value, "must be correct");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, InvalidOneByteCharacters>::value, "must be incorrect");
