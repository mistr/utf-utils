/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "test/common.hh"

template <template <char...> class IsTrue, char b0_head, char b1_head>
struct BoolAnd<IsTrue, Characters<b0_head>, Characters<b1_head>>
    : std::integral_constant<bool, IsTrue<b0_head, b1_head>::value>
{
    static_assert(IsTrue<b0_head, b1_head>::value, "must be true");
};

template <template <char...> class IsTrue, char b0_head, char b1_head, char ...b1_tail>
struct BoolAnd<IsTrue, Characters<b0_head>, Characters<b1_head, b1_tail...>>
    : std::integral_constant<bool, BoolAnd<IsTrue, Characters<b0_head>, Characters<b1_head>>::value &&
                                   BoolAnd<IsTrue, Characters<b0_head>, Characters<b1_tail...>>::value>
{ };

template <template <char...> class IsTrue, typename B1, char b0_head, char ...b0_tail>
struct BoolAnd<IsTrue, Characters<b0_head, b0_tail...>, B1>
    : std::integral_constant<bool, BoolAnd<IsTrue, Characters<b0_head>, B1>::value &&
                                   BoolAnd<IsTrue, Characters<b0_tail...>, B1>::value>
{ };

static_assert(BoolAnd<IsCorrectUtf8ByteSequence, ValidOneByteCharacters, ValidOneByteCharacters>::value, "must be correct");
static_assert(BoolAnd<IsCorrectUtf8ByteSequence, ValidTwoByteHeads, ContinuationBytes>::value, "must be correct");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidTwoByteHeads, ValidTwoByteHeads>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ContinuationBytes, ValidTwoByteHeads>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ContinuationBytes, ContinuationBytes>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidOneByteCharacters, ValidTwoByteHeads>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidTwoByteHeads, ValidOneByteCharacters>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidOneByteCharacters, ContinuationBytes>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ContinuationBytes, ValidOneByteCharacters>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidThreeByteHeads, ContinuationBytes>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidThreeByteHeads, ValidOneByteCharacters>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidThreeByteHeads, ValidTwoByteHeads>::value, "must be incorrect");
static_assert(BoolAnd<IsIncorrectUtf8ByteSequence, ValidThreeByteHeads, ValidThreeByteHeads>::value, "must be incorrect");
