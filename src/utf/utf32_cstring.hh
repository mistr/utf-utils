/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTF32_CSTRING_HH_B1E363E5DF8E6870DD1B78C3671C8BF9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UTF32_CSTRING_HH_B1E363E5DF8E6870DD1B78C3671C8BF9

#include "utf/cstring.hh"
#include "utf/utf32.hh"
#include "utf/utf8_cstring.hh"

#include <iosfwd>
#include <type_traits>

namespace Utf {

template <char32_t ...content>
class CString<Utf32EncodedSequence<content...>>
{
public:
    using SizeType = std::size_t;
    using Type = Utf32EncodedSequence<content...>;
    using WType = Type;
    using Data = const wchar_t[sizeof...(content) + 1];
    constexpr CString()noexcept
        : data_{ static_cast<wchar_t>(content)..., U'\0' }
    { }
    constexpr Data& c_str()const noexcept
    {
        return data_;
    }
    constexpr wchar_t operator[](SizeType idx)const noexcept
    {
        return data_[idx];
    }
    template <SizeType idx>
    static constexpr wchar_t at = CString()[idx];
    constexpr SizeType size()const noexcept
    {
        return sizeof...(content);
    }
    constexpr SizeType length()const noexcept
    {
        return this->size();
    }
    constexpr bool empty()const noexcept
    {
        return this->size() == 0;
    }
private:
    Data data_;
    friend std::ostream& operator<<(std::ostream& out, const CString&)
    {
        return out << CString<typename Convert<Type>::ToUtf8>();
    }
    template <typename R>
    friend constexpr bool operator==(const CString&, const CString<R>&)noexcept
    {
        return std::is_same<Type, R>::value || std::is_same<Type, typename CString<R>::WType>::value;
    }
    friend constexpr bool operator==(const CString& lhs, const wchar_t* rhs)noexcept
    {
        for (SizeType idx = 0; idx < lhs.size(); ++idx)
        {
            if (rhs[idx] == '\0')
            {
                return false;
            }
            if (lhs[idx] != rhs[idx])
            {
                return false;
            }
        }
        return rhs[lhs.size()] == '\0';
    }
    template <std::size_t N>
    friend constexpr bool operator==(const CString& lhs, const wchar_t (&rhs)[N])noexcept
    {
        if (N != (lhs.size() + 1))
        {
            return false;
        }
        for (SizeType idx = 0; idx < lhs.size(); ++idx)
        {
            if (lhs[idx] != rhs[idx])
            {
                return false;
            }
        }
        return true;
    }
    template <std::size_t N>
    friend constexpr bool operator==(const wchar_t (&lhs)[N], const CString& rhs)noexcept
    {
        return rhs == lhs;
    }
    friend constexpr bool operator==(const wchar_t* lhs, const CString& rhs)noexcept
    {
        return rhs == lhs;
    }
    template <typename R>
    friend constexpr bool operator!=(const CString& lhs, const CString<R>& rhs)noexcept
    {
        return !(lhs == rhs);
    }
    template <typename R>
    friend constexpr bool operator<(const CString& lhs, const CString<R>& rhs)noexcept
    {
        if (lhs.size() <= rhs.size())
        {
            for (SizeType idx = 0; idx < lhs.size(); ++idx)
            {
                if (lhs[idx] < rhs[idx])
                {
                    return true;
                }
                if (rhs[idx] < lhs[idx])
                {
                    return false;
                }
            }
            return lhs.size() < rhs.size();
        }
        return !(rhs < lhs);
    }
};

}//namespace Utf

#endif//UTF32_CSTRING_HH_B1E363E5DF8E6870DD1B78C3671C8BF9
