/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTF8_CSTRING_HH_2E5B44FA119801DB28D4CA8254B1D262//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UTF8_CSTRING_HH_2E5B44FA119801DB28D4CA8254B1D262

#include "utf/cstring.hh"
#include "utf/convert.hh"
#include "utf/utf8.hh"
#include "utf/utf32_length.hh"

#include <iosfwd>
#include <type_traits>

namespace Utf {

template <char ...content>
class CString<Utf8EncodedSequence<content...>>
{
public:
    using SizeType = std::size_t;
    using Type = Utf8EncodedSequence<content...>;
    using WType = typename Convert<Type>::ToUtf32;
    using Data = const char[sizeof...(content) + 1];
    constexpr CString()noexcept
        : data_{ content..., '\0' }
    { }
    constexpr Data& c_str()const noexcept
    {
        return data_;
    }
    constexpr char operator[](SizeType idx)const noexcept
    {
        return data_[idx];
    }
    template <SizeType idx>
    static constexpr auto at = detail::At<WType, idx>::value;
    template <SizeType idx>
    static constexpr auto narrow_at = detail::At<WType, idx>::narrow_value;
    constexpr SizeType size()const noexcept
    {
        return sizeof...(content);
    }
    constexpr SizeType length()const noexcept
    {
        return Length<WType>::value;
    }
    constexpr bool empty()const noexcept
    {
        return this->size() == 0;
    }
private:
    static_assert(IsCorrect<Type>::value, "must be correct utf8 byte sequence");
    Data data_;
    friend std::ostream& operator<<(std::ostream& out, const CString& s)
    {
        return out << s.c_str();
    }
    template <typename R>
    friend constexpr bool operator==(const CString&, const CString<R>&)noexcept
    {
        return std::is_same<Type, R>::value || std::is_same<WType, R>::value;
    }
    friend constexpr bool operator==(const CString& lhs, const char* rhs)noexcept
    {
        for (SizeType idx = 0; idx < lhs.size(); ++idx)
        {
            if (rhs[idx] == '\0')
            {
                return false;
            }
            if (lhs[idx] != rhs[idx])
            {
                return false;
            }
        }
        return rhs[lhs.size()] == '\0';
    }
    template <std::size_t N>
    friend constexpr bool operator==(const CString& lhs, const char (&rhs)[N])noexcept
    {
        if (N != (lhs.size() + 1))
        {
            return false;
        }
        for (SizeType idx = 0; idx < lhs.size(); ++idx)
        {
            if (lhs[idx] != rhs[idx])
            {
                return false;
            }
        }
        return true;
    }
    template <std::size_t N>
    friend constexpr bool operator==(const char (&lhs)[N], const CString& rhs)noexcept
    {
        return rhs == lhs;
    }
    friend constexpr bool operator==(const char* lhs, const CString& rhs)noexcept
    {
        return rhs == lhs;
    }
    template <typename R>
    friend constexpr bool operator!=(const CString& lhs, const CString<R>& rhs)noexcept
    {
        return !(lhs == rhs);
    }
    template <typename R>
    friend constexpr bool operator<(const CString& lhs, const CString<R>& rhs)noexcept
    {
        if (lhs.size() <= rhs.size())
        {
            for (SizeType idx = 0; idx < lhs.size(); ++idx)
            {
                if (lhs[idx] < rhs[idx])
                {
                    return true;
                }
                if (rhs[idx] < lhs[idx])
                {
                    return false;
                }
            }
            return lhs.size() < rhs.size();
        }
        return !(rhs < lhs);
    }
};

}//namespace Utf

#endif//UTF8_CSTRING_HH_2E5B44FA119801DB28D4CA8254B1D262
