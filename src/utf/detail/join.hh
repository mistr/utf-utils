/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef JOIN_HH_83776DA3911FD18F653CBD1615CC5FEC//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define JOIN_HH_83776DA3911FD18F653CBD1615CC5FEC

namespace Utf {
namespace detail {

template <typename, typename> struct Join;


template <typename C, template <typename, C...> class S, C ...prefix>
struct Join<S<C, prefix...>, S<C>>
{
    using Result = S<C, prefix...>;
};

template <typename C, template <typename, C...> class S, C ...prefix, C suffix>
struct Join<S<C, prefix...>, S<C, suffix>>
{
    using Result = S<C, prefix..., suffix>;
};

template <typename C, template <typename, C...> class S, typename P, C suffix_head, C ...suffix_tail>
struct Join<P, S<C, suffix_head, suffix_tail...>>
{
    using Result = typename Join<typename Join<P, S<C, suffix_head>>::Result,
                                 S<C, suffix_tail...>>::Result;
};


template <template <char32_t...> class S, char32_t ...prefix>
struct Join<S<prefix...>, S<>>
{
    using Result = S<prefix...>;
};

template <template <char32_t...> class S, char32_t ...prefix, char32_t suffix>
struct Join<S<prefix...>, S<suffix>>
{
    using Result = S<prefix..., suffix>;
};

template <template <char32_t...> class S, typename L, char32_t suffix_head, char32_t ...suffix_tail>
struct Join<L, S<suffix_head, suffix_tail...>>
{
    using Result = typename Join<typename Join<L, S<suffix_head>>::Result,
                                 S<suffix_tail...>>::Result;
};


template <template <char...> class S, char ...prefix>
struct Join<S<prefix...>, S<>>
{
    using Result = S<prefix...>;
};

template <template <char...> class S, char ...prefix, char suffix>
struct Join<S<prefix...>, S<suffix>>
{
    using Result = S<prefix..., suffix>;
};

template <template <char...> class S, typename L, char suffix_head, char ...suffix_tail>
struct Join<L, S<suffix_head, suffix_tail...>>
{
    using Result = typename Join<typename Join<L, S<suffix_head>>::Result,
                                 S<suffix_tail...>>::Result;
};

}//namespace Utf::detail
}//namespace Utf

#endif//JOIN_HH_83776DA3911FD18F653CBD1615CC5FEC
