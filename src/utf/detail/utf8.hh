/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTF8_HH_CAD35195537D3707255B022983B2A7BE//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UTF8_HH_CAD35195537D3707255B022983B2A7BE

#include "utf/detail/join.hh"

#include <type_traits>
#include <utility>

namespace Utf {
namespace detail {

template <char c>
struct Utf8Byte
{
    using Byte = std::make_unsigned_t<decltype(c)>;
    static constexpr Byte byte = static_cast<Byte>(c);
    template <Byte lower_bound, Byte upper_bound>
    using IsElementOfInterval = std::integral_constant<bool, (lower_bound <= byte) && (byte <= upper_bound)>;
    static constexpr bool is_head_of_one_byte_sequence = IsElementOfInterval<0b0000'0000, 0b0111'1111>::value;
    static constexpr bool is_head_of_two_byte_sequence = IsElementOfInterval<0b1100'0010, 0b1101'1111>::value;
    static constexpr bool is_head_of_three_byte_sequence = IsElementOfInterval<0b1110'0000, 0b1110'1111>::value;
    static constexpr bool is_head_of_four_byte_sequence = IsElementOfInterval<0b1111'0000, 0b1111'0100>::value;
    static constexpr bool is_continuation_byte = IsElementOfInterval<0x80, 0xbf>::value;
    static constexpr bool is_at_most_one_byte_sequence = is_head_of_one_byte_sequence;
    static constexpr bool is_at_most_two_byte_sequence = is_at_most_one_byte_sequence || is_head_of_two_byte_sequence;
    static constexpr bool is_at_most_three_byte_sequence = is_at_most_two_byte_sequence || is_head_of_three_byte_sequence;
    static constexpr bool is_at_most_four_byte_sequence = is_at_most_three_byte_sequence || is_head_of_four_byte_sequence;
};

template <char...> struct Utf8CharToUtf32Char;

template <char head>
struct Utf8CharToUtf32Char<head>
{
    static_assert(Utf8Byte<head>::is_head_of_one_byte_sequence, "must be one byte sequence");
    static constexpr char32_t value = static_cast<char32_t>(head);
    static constexpr bool is_overlong = false;
};

template <char head, char c1>
struct Utf8CharToUtf32Char<head, c1>
{
    static_assert(Utf8Byte<head>::is_head_of_two_byte_sequence, "must be two byte sequence");
    static constexpr char32_t value = (static_cast<char32_t>(head & 0b0001'1111) << 6) |
                                      static_cast<char32_t>(c1 & 0b0011'1111);
    static constexpr bool is_overlong = value < 0b1000'0000;
};

template <char head, char c1, char c2>
struct Utf8CharToUtf32Char<head, c1, c2>
{
    static_assert(Utf8Byte<head>::is_head_of_three_byte_sequence, "must be three byte sequence");
    static constexpr char32_t value = (static_cast<char32_t>(head & 0b0000'1111) << 12) |
                                      (static_cast<char32_t>(c1 & 0b0011'1111) << 6) |
                                      static_cast<char32_t>(c2 & 0b0011'1111);
    static constexpr bool is_overlong = value < 0b1000'0000'0000;
};

template <char head, char c1, char c2, char c3>
struct Utf8CharToUtf32Char<head, c1, c2, c3>
{
    static_assert(Utf8Byte<head>::is_head_of_four_byte_sequence, "must be four byte sequence");
    static constexpr char32_t value = (static_cast<char32_t>(head & 0b0000'0111) << 18) |
                                      (static_cast<char32_t>(c1 & 0b0011'1111) << 12) |
                                      (static_cast<char32_t>(c2 & 0b0011'1111) << 6) |
                                      static_cast<char32_t>(c3 & 0b0011'1111);
    static constexpr bool is_overlong = value < 0b0001'0000'0000'0000'0000;
};

template <char...> struct IsUtf8ContinuationBytesSequence;

template <char c>
struct IsUtf8ContinuationBytesSequence<c>
    : std::integral_constant<bool, Utf8Byte<c>::is_continuation_byte>
{ };

template <char c0, char ...cs>
struct IsUtf8ContinuationBytesSequence<c0, cs...>
    : std::integral_constant<bool, IsUtf8ContinuationBytesSequence<c0>::value &&
                                   IsUtf8ContinuationBytesSequence<cs...>::value>
{ };

template <typename, typename = void> struct IsCorrectUtf8Encoded;

// an empty "sequence" is correct
template <template <char...> class S>
struct IsCorrectUtf8Encoded<S<>>
    : std::true_type
{ };


// one byte "sequence", 0x00-0x7f, 0b0xxx'xxxx
template <template <char...> class S, char head>
struct IsCorrectUtf8Encoded<S<head>, std::enable_if_t<Utf8Byte<head>::is_head_of_one_byte_sequence>>
    : std::integral_constant<bool, !Utf8CharToUtf32Char<head>::is_overlong>
{ };

template <template <char...> class S, char head, char ...tail>
struct IsCorrectUtf8Encoded<S<head, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_one_byte_sequence>>
    : std::integral_constant<bool, IsCorrectUtf8Encoded<S<head>>::value &&
                                   IsCorrectUtf8Encoded<S<tail...>>::value>
{ };


// two bytes sequence, 0x80-0x7ff, 110x'xxxx 10xx'xxxx
template <template <char...> class S, char head, char c1>
struct IsCorrectUtf8Encoded<S<head, c1>, std::enable_if_t<Utf8Byte<head>::is_head_of_two_byte_sequence>>
    : std::integral_constant<bool, IsUtf8ContinuationBytesSequence<c1>::value &&
                                   !Utf8CharToUtf32Char<head, c1>::is_overlong>
{ };

template <template <char...> class S, char head, char c1, char ...tail>
struct IsCorrectUtf8Encoded<S<head, c1, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_two_byte_sequence>>
    : std::integral_constant<bool, IsCorrectUtf8Encoded<S<head, c1>>::value &&
                                   IsCorrectUtf8Encoded<S<tail...>>::value>
{ };


// three bytes sequence, 0x800-0xffff, 1110'xxxx 10xx'xxxx 10xx'xxxx
template <template <char...> class S, char head, char c1, char c2>
struct IsCorrectUtf8Encoded<S<head, c1, c2>, std::enable_if_t<Utf8Byte<head>::is_head_of_three_byte_sequence>>
    : std::integral_constant<bool, IsUtf8ContinuationBytesSequence<c1, c2>::value &&
                                   !Utf8CharToUtf32Char<head, c1, c2>::is_overlong>
{ };

template <template <char...> class S, char head, char c1, char c2, char ...tail>
struct IsCorrectUtf8Encoded<S<head, c1, c2, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_three_byte_sequence>>
    : std::integral_constant<bool, IsCorrectUtf8Encoded<S<head, c1, c2>>::value &&
                                   IsCorrectUtf8Encoded<S<tail...>>::value>
{ };


// four bytes sequence, 0x1000-0x1fffff, 1111'0xxx 10xx'xxxx 10xx'xxxx 10xx'xxxx
template <template <char...> class S, char head, char c1, char c2, char c3>
struct IsCorrectUtf8Encoded<S<head, c1, c2, c3>, std::enable_if_t<Utf8Byte<head>::is_head_of_four_byte_sequence>>
    : std::integral_constant<bool, IsUtf8ContinuationBytesSequence<c1, c2, c3>::value &&
                                   !Utf8CharToUtf32Char<head, c1, c2, c3>::is_overlong>
{ };

template <template <char...> class S, char head, char c1, char c2, char c3, char ...tail>
struct IsCorrectUtf8Encoded<S<head, c1, c2, c3, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_four_byte_sequence>>
    : std::integral_constant<bool, IsCorrectUtf8Encoded<S<head, c1, c2, c3>>::value &&
                                   IsCorrectUtf8Encoded<S<tail...>>::value>
{ };


// incorrect sequences
template <template <char...> class S, char head>
struct IsCorrectUtf8Encoded<S<head>, std::enable_if_t<!Utf8Byte<head>::is_at_most_one_byte_sequence>>
    : std::false_type
{ };

template <template <char...> class S, char head, char c1>
struct IsCorrectUtf8Encoded<S<head, c1>, std::enable_if_t<!Utf8Byte<head>::is_at_most_two_byte_sequence>>
    : std::false_type
{ };

template <template <char...> class S, char head, char c1, char c2>
struct IsCorrectUtf8Encoded<S<head, c1, c2>, std::enable_if_t<!Utf8Byte<head>::is_at_most_three_byte_sequence>>
    : std::false_type
{ };

template <template <char...> class S, char head, char c1, char c2, char c3>
struct IsCorrectUtf8Encoded<S<head, c1, c2, c3>, std::enable_if_t<!Utf8Byte<head>::is_at_most_four_byte_sequence>>
    : std::false_type
{ };

template <typename, typename = void> struct Utf8;


template <template <char...> class S, char head>
struct Utf8<S<head>, std::enable_if_t<Utf8Byte<head>::is_head_of_one_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = D<Utf8CharToUtf32Char<head>::value>;
};

template <template <char...> class S, char head, char ...tail>
struct Utf8<S<head, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_one_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = typename Join<typename Utf8<S<head>>::template To<D>,
                             typename Utf8<S<tail...>>::template To<D>>::Result;
};

template <template <char...> class S, char head, char c1>
struct Utf8<S<head, c1>, std::enable_if_t<Utf8Byte<head>::is_head_of_two_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = D<Utf8CharToUtf32Char<head, c1>::value>;
};

template <template <char...> class S, char head, char c1, char ...tail>
struct Utf8<S<head, c1, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_two_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = typename Join<typename Utf8<S<head, c1>>::template To<D>,
                             typename Utf8<S<tail...>>::template To<D>>::Result;
};

template <template <char...> class S, char head, char c1, char c2>
struct Utf8<S<head, c1, c2>, std::enable_if_t<Utf8Byte<head>::is_head_of_three_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = D<Utf8CharToUtf32Char<head, c1, c2>::value>;
};

template <template <char...> class S, char head, char c1, char c2, char ...tail>
struct Utf8<S<head, c1, c2, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_three_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = typename Join<typename Utf8<S<head, c1, c2>>::template To<D>,
                             typename Utf8<S<tail...>>::template To<D>>::Result;
};

template <template <char...> class S, char head, char c1, char c2, char c3>
struct Utf8<S<head, c1, c2, c3>, std::enable_if_t<Utf8Byte<head>::is_head_of_four_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = D<Utf8CharToUtf32Char<head, c1, c2, c3>::value>;
};

template <template <char...> class S, char head, char c1, char c2, char c3, char ...tail>
struct Utf8<S<head, c1, c2, c3, tail...>, std::enable_if_t<Utf8Byte<head>::is_head_of_four_byte_sequence>>
{
    template <template <char32_t...> class D>
    using To = typename Join<typename Utf8<S<head, c1, c2, c3>>::template To<D>,
                             typename Utf8<S<tail...>>::template To<D>>::Result;
};

}//namespace Utf::detail
}//namespace Utf

#endif//UTF8_HH_CAD35195537D3707255B022983B2A7BE
