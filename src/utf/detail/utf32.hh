/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTF32_HH_42EFBD13DBA63D6B01147CB7F367DEE2//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UTF32_HH_42EFBD13DBA63D6B01147CB7F367DEE2

#include "utf/detail/join.hh"

#include <type_traits>
#include <utility>

namespace Utf {
namespace detail {

template <char32_t c>
struct Utf32Char
{
    using Char = std::make_unsigned_t<decltype(c)>;
    static constexpr Char value = static_cast<Char>(c);
    template <unsigned n>
    struct HasAtMost
    {
        static constexpr bool bits = (value >> n) == 0;
    };
    static constexpr bool is_convertible_to_one_byte_utf8_sequence = HasAtMost<7>::bits;
    static constexpr bool is_convertible_to_two_byte_utf8_sequence = !is_convertible_to_one_byte_utf8_sequence &&
                                                                     HasAtMost<11>::bits;
    static constexpr bool is_convertible_to_three_byte_utf8_sequence = !is_convertible_to_one_byte_utf8_sequence &&
                                                                       !is_convertible_to_two_byte_utf8_sequence &&
                                                                       HasAtMost<16>::bits;
    static constexpr bool is_convertible_to_four_byte_utf8_sequence = !is_convertible_to_one_byte_utf8_sequence &&
                                                                      !is_convertible_to_two_byte_utf8_sequence &&
                                                                      !is_convertible_to_three_byte_utf8_sequence &&
                                                                      HasAtMost<21>::bits;
    static constexpr bool is_correct = is_convertible_to_one_byte_utf8_sequence ||
                                       is_convertible_to_two_byte_utf8_sequence ||
                                       is_convertible_to_three_byte_utf8_sequence ||
                                       is_convertible_to_four_byte_utf8_sequence;
};

template <typename, std::size_t, typename = void> struct At;

template <template <char32_t...> class S>
struct At<S<>, 0>
    : std::integral_constant<char32_t, U'\0'>
{
    static constexpr char narrow_value = '\0';
};

template <std::size_t idx, template <char32_t...> class S, char32_t head, char32_t ...tail>
struct At<S<head, tail...>, idx,
          std::enable_if_t<(idx == 0) && Utf32Char<head>::is_convertible_to_one_byte_utf8_sequence>>
    : std::integral_constant<char32_t, head>
{
    static constexpr char narrow_value = static_cast<char>(head);
};

template <std::size_t idx, template <char32_t...> class S, char32_t head, char32_t ...tail>
struct At<S<head, tail...>, idx,
          std::enable_if_t<(idx == 0) && !Utf32Char<head>::is_convertible_to_one_byte_utf8_sequence>>
    : std::integral_constant<char32_t, head>
{
    static constexpr char32_t narrow_value = head;
};

template <std::size_t idx, template <char32_t...> class S, char32_t head, char32_t ...tail>
struct At<S<head, tail...>, idx, std::enable_if_t<0 < idx>>
    : std::integral_constant<char32_t, At<S<tail...>, idx - 1>::value>
{
    static constexpr decltype(At<S<tail...>, idx - 1>::narrow_value) narrow_value = At<S<tail...>, idx - 1>::narrow_value;
};

template <typename> struct IsCorrectUtf32Encoded;

// an empty "sequence" is correct
template <template <char32_t...> class S>
struct IsCorrectUtf32Encoded<S<>> : std::true_type
{ };

// all utf-32 characters must be convertible to utf-8 byte sequence
template <template <char32_t...> class S, char32_t head, char32_t ...tail>
struct IsCorrectUtf32Encoded<S<head, tail...>>
    : std::integral_constant<bool, Utf32Char<head>::is_correct && IsCorrectUtf32Encoded<S<tail...>>::value>
{ };


template <char32_t, typename = void> struct Utf32CharToUtf8;

template <char32_t c>
struct Utf32CharToUtf8<c, std::enable_if_t<Utf32Char<c>::is_convertible_to_one_byte_utf8_sequence>>
{
    template <template <char...> class D>
    using Sequence = D<static_cast<char>(Utf32Char<c>::value)>;
};

template <char32_t c>
struct Utf32CharToUtf8<c, std::enable_if_t<Utf32Char<c>::is_convertible_to_two_byte_utf8_sequence>>
{
    template <template <char...> class D>
    using Sequence = D<static_cast<char>((Utf32Char<c>::value >> 6) | 0b1100'0000),
                       static_cast<char>((Utf32Char<c>::value & 0b0011'1111) | 0b1000'0000)>;
};

template <char32_t c>
struct Utf32CharToUtf8<c, std::enable_if_t<Utf32Char<c>::is_convertible_to_three_byte_utf8_sequence>>
{
    template <template <char...> class D>
    using Sequence = D<static_cast<char>((Utf32Char<c>::value >> 12) | 0b1110'0000),
                       static_cast<char>(((Utf32Char<c>::value >> 6) & 0b0011'1111) | 0b1000'0000),
                       static_cast<char>((Utf32Char<c>::value & 0b0011'1111) | 0b1000'0000)>;
};

template <char32_t c>
struct Utf32CharToUtf8<c, std::enable_if_t<Utf32Char<c>::is_convertible_to_four_byte_utf8_sequence>>
{
    template <template <char...> class D>
    using Sequence = D<static_cast<char>((Utf32Char<c>::value >> 18) | 0b1111'0000),
                       static_cast<char>(((Utf32Char<c>::value >> 12) & 0b0011'1111) | 0b1000'0000),
                       static_cast<char>(((Utf32Char<c>::value >> 6) & 0b0011'1111) | 0b1000'0000),
                       static_cast<char>((Utf32Char<c>::value & 0b0011'1111) | 0b1000'0000)>;
};


template <typename> struct Utf32;

template <template <char32_t...> class S>
struct Utf32<S<>>
{
    template <template <char...> class D>
    using To = D<>;
};

template <template <char32_t...> class S, char32_t head, char32_t ...tail>
struct Utf32<S<head, tail...>>
{
    template <template <char...> class D>
    using To = typename Join<typename Utf32CharToUtf8<head>::template Sequence<D>,
                             typename Utf32<S<tail...>>::template To<D>>::Result;
};

}//namespace Utf::detail
}//namespace Utf

#endif//UTF32_HH_42EFBD13DBA63D6B01147CB7F367DEE2
