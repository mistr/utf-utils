/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTF32_HH_5C1EBE2E90B0580129B56BC437D9034E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UTF32_HH_5C1EBE2E90B0580129B56BC437D9034E

#include "utf/is_correct.hh"
#include "utf/detail/utf32.hh"

namespace Utf {

template <char32_t ...utf32_encoded_content> struct Utf32EncodedSequence;

template <char32_t ...content>
struct IsCorrect<Utf32EncodedSequence<content...>>
    : std::integral_constant<bool, detail::IsCorrectUtf32Encoded<Utf32EncodedSequence<content...>>::value>
{ };

}//namespace Utf

#endif//UTF32_HH_5C1EBE2E90B0580129B56BC437D9034E
