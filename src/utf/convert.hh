/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CONVERT_HH_BC6613A51ABB71F47DCE9CC356B41915//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONVERT_HH_BC6613A51ABB71F47DCE9CC356B41915

#include "src/utf/utf8.hh"
#include "src/utf/utf32.hh"
#include "src/utf/cstring.hh"

#include <utility>

namespace Utf {

template <typename> struct Convert;

template <char ...utf8>
struct Convert<Utf8EncodedSequence<utf8...>>
{
    using ToUtf8 = Utf8EncodedSequence<utf8...>;
    using ToUtf32 = typename detail::Utf8<ToUtf8>::template To<Utf32EncodedSequence>;
    using ToString = CString<ToUtf8>;
    using ToWString = CString<ToUtf32>;
    struct ToNative
    {
        using Utf = ToUtf8;
        using String = ToString;
    };
    struct ToOposite
    {
        using Utf = ToUtf32;
        using String = ToWString;
    };
    static_assert(IsCorrect<ToUtf8>::value, "must be valid utf8 encoded byte sequence");
    static_assert(IsCorrect<ToUtf32>::value, "must be valid utf32 characters sequence");
};

template <char ...content>
struct Convert<std::integer_sequence<char, content...>> : Convert<Utf8EncodedSequence<content...>>
{ };

template <char32_t ...utf32>
struct Convert<Utf32EncodedSequence<utf32...>>
{
    using ToUtf32 = Utf32EncodedSequence<utf32...>;
    using ToUtf8 = typename detail::Utf32<ToUtf32>::template To<Utf8EncodedSequence>;
    using ToString = CString<ToUtf8>;
    using ToWString = CString<ToUtf32>;
    struct ToNative
    {
        using Utf = ToUtf32;
        using String = ToWString;
    };
    struct ToOposite
    {
        using Utf = ToUtf8;
        using String = ToString;
    };
    static_assert(IsCorrect<ToUtf8>::value, "must be valid utf8 encoded byte sequence");
    static_assert(IsCorrect<ToUtf32>::value, "must be valid utf32 characters sequence");
};

template <char32_t ...content>
struct Convert<std::integer_sequence<char32_t, content...>> : Convert<Utf32EncodedSequence<content...>>
{ };

template <char ...utf8>
struct Convert<CString<Utf8EncodedSequence<utf8...>>> : Convert<Utf8EncodedSequence<utf8...>>
{ };

}//namespace Utf

#endif//CONVERT_HH_BC6613A51ABB71F47DCE9CC356B41915
