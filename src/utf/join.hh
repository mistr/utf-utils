/*
 * MIT License
 *
 * Copyright (c) 2019 Michal Strnad
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef JOIN_HH_91B520740B7338AF385B4EF0A2EDA708//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define JOIN_HH_91B520740B7338AF385B4EF0A2EDA708

#include "utf/utf8.hh"
#include "utf/utf32.hh"
#include "utf/detail/join.hh"

namespace Utf {

template <typename L, typename R>
struct Join : detail::Join<L, R>
{ };

template <typename L, typename R>
struct Join<CString<L>, CString<R>>
{
    using Result = CString<typename Join<L, R>::Result>;
};

}//namespace Utf

#endif//JOIN_HH_91B520740B7338AF385B4EF0A2EDA708
